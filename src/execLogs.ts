import { upperFirst } from 'lodash';
import { info } from '@mvf/logger';
import { IPayload } from '@mvf/servicer';

const logMessageState = (
  action: string,
  state: string,
  event: string,
  payload: IPayload,
): void => {
  const actionName = `${upperFirst(action)}Action`;
  info(`Payload ${JSON.stringify(payload)}`, { actionName, state, event });
};

export default logMessageState;
