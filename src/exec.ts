import camelcaseKeys from 'camelcase-keys';
import {
  Action,
  Effect,
  IEventSource,
  InvalidEventError,
  IPayload,
} from '@mvf/servicer';
import IExecOptions from './IExecOptions';
import logMessageState from './execLogs';
import takeEffect from './takeEffect';
import { EventEmitter } from 'events';

export const run = async <THeaders extends IPayload, TBody extends IPayload>(
  _eventName: string,
  action: Action<TBody, THeaders, Effect.ITypes>,
  payload: { headers: THeaders; body: TBody },
): Promise<void> => {
  const generator = action({
    headers: camelcaseKeys(payload.headers, { deep: true }) as THeaders,
    body: camelcaseKeys(payload.body, { deep: true }) as TBody,
  });

  const em = new EventEmitter();
  let value: unknown;
  em.on('nextValue', (nextValue) => {
    value = nextValue;
  });

  while (true) {
    const next = await generator.next(value);
    if (next.done) {
      break;
    }

    await takeEffect(em, next.value);
  }
};

const runWithLogs = async <THeaders extends IPayload, TBody extends IPayload>(
  eventName: string,
  action: Action<TBody, THeaders, Effect.ITypes>,
  payload: { headers: THeaders; body: TBody },
): Promise<void> => {
  logMessageState(action.name, 'STARTED', eventName, payload);
  await run(eventName, action, payload);
  logMessageState(action.name, 'COMPLETED', eventName, payload);
};

export const buildExec =
  <THeaders extends IPayload, TBody extends IPayload>(
    op: (
      eventName: string,
      action: Action<TBody, THeaders, Effect.ITypes>,
      payload: { headers: THeaders; body: TBody },
    ) => Promise<void>,
  ) =>
  async (
    eventName: string,
    option: IExecOptions,
    source: IEventSource<any, THeaders, TBody>,
  ): Promise<void> => {
    const action = source[eventName];
    if (!action) {
      throw new InvalidEventError(eventName, source as IEventSource<any, any>);
    }

    await op(eventName, action, {
      headers: JSON.parse(option.headers) as unknown as THeaders,
      body: JSON.parse(option.body) as unknown as TBody,
    });
  };

const exec = buildExec(runWithLogs);

export default exec;
