/* eslint-disable @typescript-eslint/no-empty-function */
import { IEventSources, IEventSource } from '@mvf/servicer';

export const eventSource: IEventSource<any> = {
  async *test() {},
};

export const eventSources: IEventSources = {
  command: eventSource,
};
