import inquirer from 'inquirer';
import { keys } from 'lodash';

interface IInitialQuestionAnswers {
  explore: boolean;
}

export const initialQuestion = async (message: string): Promise<boolean> => {
  const { explore } = await inquirer.prompt<IInitialQuestionAnswers>([
    {
      type: 'confirm',
      name: 'explore',
      message: `${message}, would you like to use interactive explorer instead?`,
      suffix:
        'set EXEC_FEATURE_EXPLORER=ENABLED env to always use explorer by default',
      default: false,
    },
  ]);

  return explore;
};

interface IExploreSourcesAnswers {
  source: string;
}

export const exploreSources = async (
  eventSources: Record<string, unknown>,
): Promise<string> => {
  const { source } = await inquirer.prompt<IExploreSourcesAnswers>([
    {
      type: 'list',
      name: 'source',
      message:
        'Which of the following event sources would you like to explore?',
      choices: keys(eventSources),
    },
  ]);

  return source;
};

interface IExploreEventsAnswers {
  event: string;
}

export const exploreEvents = async (
  events: Record<string, unknown>,
): Promise<string> => {
  const { event } = await inquirer.prompt<IExploreEventsAnswers>([
    {
      type: 'list',
      name: 'event',
      message: 'Which of the following events would you like to trigger?',
      choices: keys(events),
    },
  ]);

  return event;
};
