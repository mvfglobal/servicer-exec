import IExecOptions from './IExecOptions';

interface IParams extends IExecOptions {
  source?: string;
  event?: string;
}

export default IParams;
