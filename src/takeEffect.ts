import snakeCaseKeys from 'snakecase-keys';
import { info } from '@mvf/logger';
import { IPayload, Effect } from '@mvf/servicer';
import { pause } from '@mvf/servicer/EffectResults';
import { EventEmitter } from 'events';

let terminate = false;
process.on('SIGTERM', () => {
  info(`received signal to terminate command`);
  terminate = true;
});

const takeEffect = async (
  em: EventEmitter,
  effect: Effect.ITypes,
): Promise<IPayload | void> => {
  if (!effect) {
    return;
  }

  switch (effect.type) {
    case Effect.Type.OUTPUT: {
      const data = { data: effect.payload.output };
      info(JSON.stringify(snakeCaseKeys(data, { deep: true })));
      return;
    }
    case Effect.Type.STATUS: {
      info(`${effect.payload.status}`);
      break;
    }
    case Effect.Type.PAUSE: {
      if (effect.payload.seconds > 0) {
        await new Promise((resolve) =>
          setTimeout(resolve, effect.payload.seconds * 1000),
        );
      } else {
        await new Promise((resolve) => setImmediate(resolve));
      }

      em.emit('nextValue', pause(terminate));

      break;
    }
    default: {
      break;
    }
  }
};

export default takeEffect;
