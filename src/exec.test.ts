/* eslint-disable @typescript-eslint/no-empty-function */
import * as Logger from '@mvf/logger/client';
import { InvalidEventSourceError, InvalidEventError } from '@mvf/servicer';
import exec from './exec';
import execSource from './execSource';
import { eventSource, eventSources } from './exec.mock';

describe('exec', () => {
  const spyExit = jest.spyOn(process as any, 'exit');
  spyExit.mockImplementation(() => {});
  const spyAction = jest.spyOn(eventSources.command, 'test');
  const spyLogger = jest.spyOn(Logger, 'info');

  beforeEach(() => {
    spyExit.mockClear();
    spyLogger.mockClear();
    spyAction.mockReset();
    spyAction.mockImplementation(async function* mock() {});
  });

  describe('execSource', () => {
    it('should throw InvalidEventSourceError if invalid source is provided', async () => {
      await expect(
        execSource(eventSources)('invalid', 'test', {
          headers: '{}',
          body: '{}',
        }),
      ).rejects.toThrow(InvalidEventSourceError);
    });
  });

  describe('events', () => {
    it('should throw InvalidEventError if invalid event is provided', async () => {
      await expect(
        exec('invalid', { headers: '{}', body: '{}' }, eventSource),
      ).rejects.toThrow(InvalidEventError);
    });
  });

  it('should call specified action if valid event source and event is provided', async () => {
    await exec('test', { headers: '{}', body: '{}' }, eventSource);
    expect(spyAction).toHaveBeenCalled();
  });

  it('should pass headers into the action', async () => {
    await exec('test', { headers: '{"a":1}', body: '{}' }, eventSource);
    expect(spyAction.mock.calls[0][0].headers).toEqual({ a: 1 });
  });

  it('should pass body into the action', async () => {
    await exec('test', { headers: '{}', body: '{"a":1}' }, eventSource);
    expect(spyAction.mock.calls[0][0].body).toEqual({ a: 1 });
  });

  it('should throw error if action throws an error', async () => {
    spyAction.mockImplementation(async function* mock() {
      throw new Error('Failing action');
    });
    await expect(
      exec('test', { headers: '{}', body: '{}' }, eventSource),
    ).rejects.toThrow();
  });

  it('should log the message state when started and completed', async () => {
    await exec('test', { headers: '{"a":1}', body: '{}' }, eventSource);
    expect(spyLogger).toHaveBeenCalledTimes(2);
  });

  it('should not log the message state as completed if action throws an error', async () => {
    spyAction.mockImplementation(async function* mock() {
      throw new Error('Failing action');
    });
    await exec('test', { headers: '{}', body: '{}' }, eventSource).catch(
      () => {},
    );
    expect(spyLogger).toHaveBeenCalledTimes(1);
  });
});
