import { IEventSources, IPayload, ICommand, Argv } from '@mvf/servicer';
import { error } from '@mvf/logger';
import { exploreEvents, exploreSources, initialQuestion } from './explorer';
import IParams from './IParams';
import execSource from './execSource';

class ExecCommand<THeaders extends IPayload, TBody extends IPayload>
  implements ICommand
{
  private name: string;

  private features = {
    useExplorerByDefault: false,
  };

  constructor(private eventSources: IEventSources<THeaders, TBody>) {
    this.name = 'exec';

    this.features.useExplorerByDefault =
      process.env.EXEC_FEATURE_EXPLORER === 'ENABLED';
  }

  setup(program: Argv): Argv {
    return program.command(
      `${this.getName()} [source] [event]`,
      'Execute the specified action from the specified event source',
      this.builder,
    );
  }

  command = async (args: IParams): Promise<void> => {
    const { headers, body } = args;

    let { source, event } = args;

    if (!source || !(source in this.eventSources)) {
      if (!this.features.useExplorerByDefault) {
        const explore = await initialQuestion(
          'You did not specify a valid event source',
        );

        if (!explore) {
          return;
        }
      }

      source = await exploreSources(this.eventSources);
      event = await exploreEvents(this.eventSources[source]);
    }

    const events = this.eventSources[source];
    if (!event || !(event in events)) {
      if (!this.features.useExplorerByDefault) {
        const explore = await initialQuestion(
          'You did not specify a valid event',
        );

        if (!explore) {
          return;
        }
      }

      event = await exploreEvents(this.eventSources[source]);
    }

    await this.run(source, event, headers, body);
  };

  getName = (): string => this.name;

  private run = async (
    source: string,
    event: string,
    headers: string,
    body: string,
  ): Promise<void> => {
    try {
      await execSource(this.eventSources)(source, event, { headers, body });
    } catch (err) {
      error((err as Error).message, {
        source,
        event,
        state: 'FAILED',
        err,
      });
      process.exit(1);
    }
  };

  private builder = (args: Argv): Argv => {
    args.positional('source', {
      describe: 'specifies the event source',
      default: undefined,
    });

    args.positional('event', {
      describe: 'specifies the event to be triggered',
      default: undefined,
    });

    args.option('headers', {
      describe: 'json representing headers of the action',
      default: '{}',
    });

    args.option('body', {
      describe: 'json representing body of the action',
      default: '{}',
    });

    return args;
  };
}

export default ExecCommand;
