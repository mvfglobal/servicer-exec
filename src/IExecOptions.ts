interface IExecOptions {
  headers: string;
  body: string;
}

export default IExecOptions;
