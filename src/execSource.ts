/* eslint-disable @typescript-eslint/no-unsafe-call */
import {
  IEventSources,
  InvalidEventSourceError,
  IPayload,
} from '@mvf/servicer';
import IExecOptions from './IExecOptions';
import exec from './exec';

const execSource =
  <THeaders extends IPayload, TBody extends IPayload>(
    sources: IEventSources<THeaders, TBody>,
  ) =>
  async (
    sourceName: string,
    eventName: string,
    option: IExecOptions,
  ): Promise<void> => {
    const source = sources[sourceName];
    if (!source) {
      throw new InvalidEventSourceError(sourceName, sources);
    }
    await exec(eventName, option, source);
  };

export default execSource;
