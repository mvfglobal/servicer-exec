> Migrated to monorepo https://bitbucket.org/mvfglobal/rhino-monorepo/src/master/packages/servicer-exec/

# MVF Servicer Exec

## Usage

TODO

### To install the package

Run `npm install @mvf/servicer-exec`

### Configuration

Set the following environment variables in your project

- `ENVIRONMENT_FILE` should be set to `development` | `testing` | `staging` | `production`


## Servicer Exec
To setup Servicer Exec you will need an entrypoint.ts or equivalent file.
```ts
import { Application } from "@mvf/servicer";
import { ExecCommand } from "@mvf/servicer-exec";
import { GenericEventSources } from "./EventSources/GenericEventSources";

const setupApplication = async (): Promise<void> => {
   const application = new Application();

   application.addCommands(
           new ExecCommand(GenericEventSources),
   );

   await application.run();
};

void setupApplication();
```


## Contributing

### Setup

- Run `make` to build the container
- Run `make shell` to enter the container
- Run `npm install` to install dependencies

Refer to package.json for commands

### After merging

After you have merged a PR to master, you need to rebuild and publish your changes.

1. Checkout master `git checkout master && git pull`
2. Use one of the following make publish commands to publish changes:
   - `make publish kind=patch` - Use this if your change is a bug fix and is backwards compatible.
   - `make publish kind=minor` - Use this if your change adds new functionality and is backwards compatible.
   - `make publish kind=major` - Use this if your change is not backwards compatible.
