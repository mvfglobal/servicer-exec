module.exports = {
  testEnvironment: 'node',
  roots: [
    '<rootDir>/dist/',
  ],
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.jsx?$',
};
